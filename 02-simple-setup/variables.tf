variable "name" {
  description = "The name of the system"
  default     = "simple-insecure"
}

variable "aws_region" {}
variable "vpc_cidr_block" {}
variable "application" {}
variable "iac-repo" {}

variable "script" {
  default = "simple-insecure"
}

variable "instance_type" {
    default = "t3.nano"
}
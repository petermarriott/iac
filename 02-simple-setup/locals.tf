locals {
  common_tags = {
    env                 = terraform.workspace
    application         = var.application
    iac-repo            = var.iac-repo
    script              = var.script
    terraform           = "true"
  }
}

data "aws_vpc" "selected" {
  tags = {
    Name = "${terraform.workspace}-network"
  }
}

data "aws_subnet" "selected_b" {
  tags = {
    Name = "${terraform.workspace}-network-public-${var.aws_region}b"
  }
}

data "aws_ami" "ubuntu" {
most_recent = true
owners = ["099720109477"] # Canonical

  filter {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }
}

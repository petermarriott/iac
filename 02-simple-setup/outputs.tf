output "ec2_server_01_ip" {
  description = "ec2 server 01 ip"
  value       = aws_instance.ec2_server_01.public_ip 
}

output "ec2_server_01_public_dns" {
  description = "ec2 server 01 public_dns"
  value       = aws_instance.ec2_server_01.public_dns 
}


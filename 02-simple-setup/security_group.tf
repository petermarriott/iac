resource "aws_security_group" "ssh_external_public" {
  name        = "${terraform.workspace}-ssh-external-public"
  description = "SSH Public Access"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Any IP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

    tags = merge(
              local.common_tags,
              {"Name" = "${terraform.workspace}-ssh-external-public-access"}
          )
}

resource "aws_security_group" "postgres_external_access" {
  name        = "${terraform.workspace}-postgres-external-access"
  description = "Allow inbound traffic"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    description = "Any IP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
              local.common_tags,
              { "Name" = "${terraform.workspace}-postgres-external-access" }
          )
}

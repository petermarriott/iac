resource "aws_instance" "ec2_server_01" {

  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  associate_public_ip_address = true
  availability_zone = "${var.aws_region}b"
  subnet_id = data.aws_subnet.selected_b.id
  vpc_security_group_ids = [ aws_security_group.ssh_external_public.id ]
  key_name = aws_key_pair.iac_training.key_name
  user_data = file("${path.module}/../files/userdata.webserver")
  tags = merge(
              local.common_tags,
              { "Name" = "${terraform.workspace}-ec2-server-01" }
          )
}

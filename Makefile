SHELL := /bin/bash
environment=training
aws_profile=training

.PHONY: 01-networking 02-simple-setup 

01-networking:
	export AWS_PROFILE=${aws_profile} && \
	cd 01-networking/ && \
	terraform init && \
	terraform workspace new ${environment} || true && \
	terraform workspace select ${environment}  && \
	terraform apply -var-file ../environment-config/${environment}.json

02-simple-setup:
	export AWS_PROFILE=${aws_profile} && \
	cd 02-simple-setup/ && \
	terraform init && \
	terraform workspace new ${environment} || true && \
	terraform workspace select ${environment}  && \
	terraform apply -var-file ../environment-config/${environment}.json

aws-destroy-all:
	export AWS_PROFILE=${aws_profile} && \
	cd 01-networking/ && \
	terraform workspace select ${environment}  && \
	terraform destroy -var-file ../environment-config/${environment}.json

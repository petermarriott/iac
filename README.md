# Data terraform scripts

## Before you start

*WARNING*: This repo contains scripts that create, manage, destroy live environments.

You will need the following:

- Experience using Linux.
- Knowledge of the environments you wish to control resources in.
- Terraform version 0.13.0 or above [https://www.terraform.io/downloads.html](https://www.terraform.io/downloads.html)
- awscli `sudo apt install awscli`
- AWS Access keys for the accounts you wish to control resources in
    - If you are accessing multiple AWS environments make sure you using the correct profile e.g. `export AWS_PROFILE=training`
- Permission to checkout this git repo

To debug what is going on access to:

- Access to AWS console and Parameter store.

## Assumptions

All state will be held locally as this is not for production

## Terraform

### Initial set-up

The Makefile can be used to run terraform for each of the projects

Using terraform modules from their registry e.g. [https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.48.0](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.48.0)

### Running make for setting up environment

```bash
 make 01-networking 
 ```

### 02-simple-insecure set-up

To generate a new ssh key in a bash shell:

```bash
ssh-keygen -b 2048 -t rsa -f ~/.ssh/iac_training -q -N ""
```

Remove `_change` from filenames and add your details in:

- `02-simple-insecure/key_pair.tf_change` your public ssh key
- `02-simple-insecure/fixed_ip_addresses.tf_change` your fixed ip address or VPN ip address

```bash
make 02-simple-insecure
```

Log onto the machine

- `ssh -i ~/.ssh/iac_training ubuntu@ec2-xxx-xxx-xxx-xxx.compute-1.amazonaws.com`

Check the userdata install logs:

- `cat /var/log/cloud-init-output.log`
variable "name" {
  description = "The name of the system"
  default     = "network"
}

variable "aws_region" {}
variable "vpc_cidr_block" {}
variable "application" {}
variable "iac-repo" {}

variable "script" {
  default = "networking"
}

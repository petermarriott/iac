module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.62.0"

  name = "${terraform.workspace}-${var.name}"
  cidr = var.vpc_cidr_block

  azs              = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  public_subnets   = [cidrsubnet(var.vpc_cidr_block, 8, 150), cidrsubnet(var.vpc_cidr_block, 8, 151), cidrsubnet(var.vpc_cidr_block, 8, 152)]
  private_subnets  = [cidrsubnet(var.vpc_cidr_block, 8, 200), cidrsubnet(var.vpc_cidr_block, 8, 201), cidrsubnet(var.vpc_cidr_block, 8, 202)]
  database_subnets = [cidrsubnet(var.vpc_cidr_block, 8, 220), cidrsubnet(var.vpc_cidr_block, 8, 221), cidrsubnet(var.vpc_cidr_block, 8, 222)]
  redshift_subnets = [cidrsubnet(var.vpc_cidr_block, 8, 240), cidrsubnet(var.vpc_cidr_block, 8, 241), cidrsubnet(var.vpc_cidr_block, 8, 242)]

  enable_nat_gateway = false
  # single_nat_gateway = true - save money on only having one if we enable_nat_gateway
  enable_vpn_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_s3_endpoint       = false
  enable_dynamodb_endpoint = false

  tags = merge(
              local.common_tags,
              {"module" = "terraform-aws-vpc"}
          )
}

resource "aws_subnet" "data_dmz_subnet_a" {
  vpc_id     = module.vpc.vpc_id
  cidr_block = cidrsubnet(var.vpc_cidr_block, 8, 99)
  map_public_ip_on_launch = true
  availability_zone = "${var.aws_region}a"

  tags = merge(
              local.common_tags,
              {"Name" = "${terraform.workspace}-data-dmz-subnet-a"}
          )
}

resource "aws_route_table" "dmz_route_table" {
  vpc_id     = module.vpc.vpc_id

  tags = merge(
              local.common_tags,
              {"Name" = "${terraform.workspace}-dmz-route"}
          )
}

resource "aws_route_table_association" "dmz_association_subnet_a" {
  subnet_id      = aws_subnet.data_dmz_subnet_a.id
  route_table_id = aws_route_table.dmz_route_table.id
}

resource "aws_route" "external" {
  route_table_id         = aws_route_table.dmz_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = module.vpc.igw_id
}

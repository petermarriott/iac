#VPC

output "vpc_id" {
  description = "The ID of the VPC"
  value       = module.vpc.vpc_id
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = module.vpc.public_subnets
}

output "public_cidrs" {
  description = "List of IDs of public subnet's cidr blocks"
  value       = module.vpc.public_subnets_cidr_blocks
}

output "redshift_subnets" {
  description = "List of IDs of redshift subnets"
  value       = module.vpc.redshift_subnets
}

output "redshift_cidrs" {
  description = "List of IDs of redshift subnet's cidr blocks"
  value       = module.vpc.redshift_subnets_cidr_blocks
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value       = module.vpc.database_subnets
}

output "database_cidrs" {
  description = "List of IDs of database subnet's cidr blocks"
  value       = module.vpc.database_subnets_cidr_blocks
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = module.vpc.private_subnets
}

output "private_cidrs" {
  description = "List of IDs of private subnet's cidr blocks"
  value       = module.vpc.private_subnets_cidr_blocks
}
